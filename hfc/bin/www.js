/**
 * Module dependencies.
 */

var app = require('../app');
var debug = require('debug')('express_api:server');
var http = require('http');
var yaml = require('js-yaml');
var fs   = require('fs');

/**
 * Setting Model
 */
var config = require('../config/database-config.json');
var Obchain = require('../obchain_modules/obchain');

// db , redis 설정 까지 하고, connect 
var db = Obchain.newDatabase(config.username, config.password, config.databaseName, config.ip);
db.connect();


// config에 있는 network-config 정보 가져온다 
try {
	var config = yaml.safeLoad(fs.readFileSync('./config/network-config.yaml', 'utf8'));
} catch (e) {
	throw new Error(e);
}

var peerArr = [];

// key는 config의 채널 정보를 가져온다
// 각 채널의 peer, orderer 정보를 obchain tmpNode에 담고
var keys = Object.keys(config.channels);
for(var idx in keys) {
	var peer = Object.keys(config.channels[keys[idx]].peers)[0];
	var orderer = config.channels[keys[idx]].orderers[0];
  var tmpNode = Obchain.newNode(keys[idx], peer, orderer);
  peerArr.push(tmpNode);
 	Obchain.pushNodeArray(tmpNode);
}

global.peerArr = peerArr;
//Obchain.setNodeArray([peerArr[1]])

/**
 * Get port from environment and store in Express.
 */

require('../config/config.js');
var hfc = require('../obchain_modules/fabric-client');



/*포트, host 설정 */
var host = process.env.HOST || hfc.getConfigSetting('host');
var port = process.env.PORT || hfc.getConfigSetting('port');

app.set('port', port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);






/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}
