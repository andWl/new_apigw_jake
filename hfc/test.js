




// // 변수 및 배열 및 정렬 
// console.log('hello world');

// var temp = 1
// console.log(temp,"\n")

// var li = ['a', 'b', 'c', 'd', 'e'];
// console.log("추가전길이:",li.length,"\n")
// li.push('f');
// console.log(li);
// console.log("추가후길이,",li.length,"\n")

// var li = ['a', 'b', 'c', 'd', 'e'];
// li=li.concat(["f","g"]);//push는 그냥 li.push("f"), concat은 대입해야되고 [] 써야됨
// console.log("concat", li,"\n");

// var li = ['a', 'b', 'c', 'd', 'e'];
// li.unshift('z');//z, a, b, c, d, e 가 출력// index가 하나씩 밀린다
// console.log("unshift", li,"\n");

// var li = ['a', 'b', 'c', 'd', 'e'];
// li.splice(2, 2, 'B')//a,b,B,e : 인덱스값이2인 원소포함 뒤로 2개 삭제 후 앞에 B를 삽입
// console.log("splice1", li,"\n");

// var li = ['a', 'b', 'c', 'd', 'e'];
// li.splice(2, 0, 'B','C');//,a,b,B,C,c,d,e  : 인덱스값이2인 원소포함 뒤로 0개 삭제 후 앞에 B를 삽입
// console.log("splice2", li,"\n");

// var li = ['a', 'b', 'c', 'd', 'e'];
// li.shift(); //b,c,d,e 앞에꺼하나 삭제
// console.log("shift",li,"\n");

// var li = ['a', 'b', 'c', 'd', 'e'];
// li.pop();
// console.log("pop", li,"\n");// a,b,c,d, 뒤에꺼하나 삭제 

// var li = ['c', 'e', 'a', 'b', 'd'];//a,b,c,d,e
// li.sort();//정렬
// console.log("sort", li,"\n");

// var li = ['c', 'e', 'a', 'b', 'd'];//d,b,a,e,c 그냥 정렬이 아니라 거꾸로 뒤집기만한것
// li.reverse();//역순정렬
// console.log("reverse", li,"\n");

// var member = ['egoing', 'k8805', 'sorialgi']
// console.log(member[0]);//0 : 인덱스
// console.log(member[1]);
// console.log(member[2]);






/* parseint    */
//// 문자열을 저장하고 있는 변수를 정수로 변환한 값을 얻으려면 parseInt(), 실수로 변환한 값을 얻으려면 parseFloat()과 같은 함수를 사용해야 한다. 위 문제를 parseInt() 함수를 이용하여 수정한 코드는 다음과 같다.
// //Promp는 문자열로 반환하기때문

// var str1 = "10"
// var str2 = "20"
// var num1 = parseInt(str1);
// var num2 = parseInt(str2);
// console.log(typeof(str1));
// console.log(typeof(num1));
// console.log(num1 + num2);




// /*toUpperCase */
// function get_members(){
//     return ['egoing', 'k8805', 'sorialgi'];
// }
// members = get_members();

// for(i = 0; i < members.length; i++){  // members.length는 배열에 담긴 원소의 갯수를 알려준다.
  
//     console.log(members[i].toUpperCase()+"<br/>");
// // members[i].toUpperCase()는 members[i]에 담긴 문자를 대문자로 변환해준다. 이런걸 내장함수라한다
// }





// // 비교
// console.log(a);//undefined 값이 없는상황
// var a=null;
// console.log(a);// undefined .개발자가 일부러 null값 넣은것

// console.log(undefined == null);//true
// console.log(undefined === null);//false

// console.log(null == undefined);       //true
// console.log(null === undefined);      //false
// console.log(true == 1);               //true
// console.log(true === 1);              //false
// console.log(true == '1');             //true
// console.log(true === '1');            //false
 
// console.log(0 === -0);                //true
// console.log(NaN === NaN);             //false

// if(!''){//''는 false !''는 true
//     console.log('빈 문자열')//출력
// }
// if(!undefined){
//     console.log('undefined');//출력
// }
// var a;
// if(!a){
//     console.log('값이 할당되지 않은 변수');//출력
// }
// if(!null){
//     console.log('null');//출력
// }
// if(!NaN){
//     console.log('NaN');//출력
// }   







// // if else
// if(true){
//     console.log('asfsdf ');
// }

// if(false){
//     console.log('nononono');
// }//아무것도 출력하지 않는다!!


// if(false){
//     console.log(1);
// } else if(true){
//     console.log(2);
// } else if(true){
//     console.log(3);
// } else {
//     console.log(4);
// }
// //결과 2








/*continue , break  */
// for(var i = 0; i < 10; i++){ //var로 i초기화
//     if(i === 5) {
//         break;
//     }
//     console.log('coding everybody'+i+'<br />');// 5는 생략됨 
// }
 
// for(var i = 0; i < 10; i++){
//     if(i === 5) {
//         continue;
//     }
//     console.log('coding everybody'+i+'<br />'); // 5가 생략 
// }
 







//// 함수
// var numbering = function (){
//     i = 0;
//     while(i < 10){
//         console.log(i);
//         i += 1;
//     }
// }
// numbering();//이렇게도 함수 표현 가능 



// function get_argument(arg){//arg: 매개변수, parameter, 출력!
//         return arg*1000;
//     }

//     console.log(get_argument(1));//1000 -> 1 :인자, argument , 입력!!
//     console.log(get_argument(2));//2000

// function get_arguments(arg1, arg2){
//     return arg1 + arg2 // return 값은 하나만 가질 수 있다
// }

// console.log(get_arguments(10, 20));
// console.log(get_arguments(20, 30));





// //객체 
// var grades = {'egoing': 10, 'k8805': 6, 'sorialgi': 80};
// //객체 생성은 중괄호!, 객체는 배열의 인덱스 0,1,2 대신 이름넣는거
//     //egoing :key , 10: value
//     console.log(grades['egoing']);//불러오는법

//     console.log(grades["sorialgi"]);//=grades['sorial'+'gi"]; 이런식으로 프로그래밍 적으로 생성가능. []방식으로 써야할때가 있음
//     console.log(grades.sorialgi);//불러오는법 이게 더 나은듯, !=grades.sorial+gi 안됨


// var grades = {};//다른법
// grades['egoing'] = 10;// 
// grades['k8805'] = 6;
// grades['sorialgi'] = 80;

// console.log(grades['egoing']);//불러오는법

// console.log(grades["sorialgi"]);//=grades['sorial'+'gi"]; 이런식으로 프로그래밍 적으로 생성가능. []방식으로 써야할때가 있음
// console.log(grades.sorialgi);//불러오는법 이게 더 나은듯, !=grades.sorial+gi 안됨


// var grades = new Object();//다른법
// grades['egoing'] = 10;
// grades['k8805'] = 6;
// grades['sorialgi'] = 80;

// console.log(grades['egoing']);//불러오는법

// console.log(grades["sorialgi"]);//=grades['sorial'+'gi"]; 이런식으로 프로그래밍 적으로 생성가능. []방식으로 써야할때가 있음
// console.log(grades.sorialgi);//불러오는법 이게 더 나은듯, !=grades.sorial+gi 안됨




/*for 객체 */
// var grades = {'egoing': 10, 'k8805': 6, 'sorialgi': 80};
// for(key in grades){//여기서 key는 바꿀수 있다 아무거나 이름
//     console.log("key:"+key);//egoing, k8805, sorialgi 출력 이쁘게 출력됨 들여쓰기해서
// }


// var grades = {'egoing': 10, 'k8805': 6, 'sorialgi': 80};
// for(key in grades) {//객체에서 for 문 돌릴때
//     console.log("key : "+key+" value : "+grades[key]);
//     console.log(grades[key]+1);
// }




/* 이중 객체( 객체 안에 객체랑 함수 넣은 구조 )*/
// var grades = {
//     'list': {'egoing' : 10, 'k8805' : 8, 'sorialgi' : 80},
//     'show': function () {
//         console.log("hello world"); //객체엔 함수도 저장될수있다. 자바스크립트에서는 함수도 일종의 값이다-> 함수도 변수에 저장될수 있다. 
//     }
// }
// console.log(grades.list.egoing)//alert(grades['list']['egoing']); 이랑 같다 10출력
// grades.show()//grades['show'](); 이랑같다 hello world 출력 함수 불러올때는 앞에 alert 쓰면 안됨


// for(key in grades.list) {
//     console.log(grades.list[key])
// }



/*객체지향 프로그래밍 / 객체안에 함수 밑 for문 / this  */
// var grades = {
//     //이렇게 서로 연관되어있는 데이터와 연관 되어 있는 처리를 하나의
//     // 그릇안에 모아서 그룹화 해놓은 프로그래밍 기법이 객체지향 프로그래밍
//     'list': {'egoing': 10, 'k8805': 8, 'sorialgi': 80},
//     'show': function () {
//         for (var key in this.list) {
//             console.log(key+" ", this.list[key]);//this는 약속되어있는 변수다.
//             // 자바스크립트의 정해져있는 변수이다. 즉, 이 함수가 속해있는 객체를 가리키는 변수
//             //즉 여기서는 grades를 가리킨다. this를 grades 로 바꿔도 돌아간다. 
//         }
//     }
// }
// grades.show();




/* 모듈화 장점 */

// 프로그램은 작고 단순한 것에서 크고 복잡한 것으로 진화한다. 그 과정에서 코드의 재활용성을 높이고, 유지보수를 쉽게 할 수 있는 다양한 기법들이 사용된다. 그 중의 하나가 코드를 여러 개의 파일로 분리하는 것이다. 이를 통해서 얻을 수 있는 효과는 아래와 같다.
// ◦자주 사용되는 코드를 별도의 파일로 만들어서 필요할 때마다 재활용할 수 있다.
// ◦코드를 개선하면 이를 사용하고 있는 모든 애플리케이션의 동작이 개선된다.
// ◦코드 수정 시에 필요한 로직을 빠르게 찾을 수 있다.
// ◦필요한 로직만을 로드 해서 메모리의 낭비를 줄일 수 있다.
// ◦한번 다운로드 된 모듈은 웹 브라우저에 의해서 저장되기 때문에 동일한 로직을 로드 할 때 시간과 네트워크 트래픽을 절약 할 수 있다. (브라우저에서만 해당)



/* let , var, const */
// 유연성의 대명사 var vs 깐깐한 let, const
// JavaScript에서 ES6로 넘어오면서, let과 const 까지도 알아야하는데요.
// 특히나 요즘 나오는 예제 코드들을 보면 let과 const를 활용하고 있으므로,
// 이제는 유연한(?) var만으로는 버티기 어렵게 되었습니다.

// 아래와 같은 변수에 var를 두번 선언하도라도 가장 마지막에 선언한 변수에 담긴 값으로 에러없이 출력됩니다.

// var test = "123"
// var test = "2323"
// console.log(test)







// //  var , let, const 함수 범위 
// 사실 var 그리고 let, const를 사용함에 있어서 가장 중요한 부분 중 하나인데요. 
// var로 선언한 변수의 범위는 함수의 범위(function scope)를 가지고 있습니다.
// 하나의 함수내에서는 적용범위가 유효하게 됩니다.

// 하지만, let과 const는 블록단위(block scope)의 범위를 가지고 있어서,
// {}로 감싸고 있는 범위내에서 유효합니다.

// 변수의 범위가 다른 것이 어떻게 영향을 미치는지 보도록 하겠습니다.
// 기존의 var를 사용하던 개념으로 생각해보면, isAdult는 true 를 console창에 표시해 줄 것 같은데요.
// (실제로도 var를 사용하면 true를 출력하는 것을 확인할 수 있습니다.)

// 결과는 아래의 이미지에서 보시다시피, false가 찍혀 있습니다.
// 이것은 위의 isAdult와 if문안의 isAdult의 범위(scope)가 서로 다르기 때문인데요.
// 두개의 isAdult가 각각 다른 범위에서 존재하고 있는 것이지요.
// // 
// 함수범위에서 광범위하게 존재하고 유연성있게 사용하는 var는,
// bug를 발생시킬 수 있다는 공격도 항상 받아왔는데요.
// 이제 let과 const를 사용하면 되겠습니다.


// const name= "brown"
// let age = 21;
// let isAdult = false;

// if (age > 18){
//     let isAdult = true
// }

// console.log(isAdult)






/*let . const 차이  */
// // 
// 먼저 유추하기 쉬운 const를 먼저 보겠습니다.
// 상수라는 영어의 constance로부터 온 것 같은 const는
// Primitive 타입인 string, number, boolean, null, undefined의 상수 선언에 사용되어집니다. 
// 한번 선언하고 다시 선언하면 에러가 난다는 것이죠.
// 상수를 선언하는데 const를 사용하고 변수를 사용하는데 let을 사용하면 되겠군요.

// 아래와 같이 한번 선언한 const변수에 다시 값을 넣어보려고 하면 에러가 발생하는 것을 볼 수 있습니다.
// 반면에 let의 경우는 아래와 같이 에러를 발생시키지 않는 것을 볼 수 있는데요.
// let에는 const와는 반대로, 변화되는 값을 대입할 수 있다는 사실을 알 수 있겠지요.


// const constTest = "constTest1";
// constTest = "isChangeable"
// console.log(letTest)  // 에러 발생

// let letTest = "letTest1";
// letTest = "isChangeable"
// console.log(letTest)  //변경 가능 



/* const 객체의 property는 변경가능 */
// 
// 상수선언에 사용한다고 하니, 절대 변하지 않는다고 생각할 수도 있지만, 그렇지 않습니다.
// 상수로 선언한 객체변수의 property는 변할 수 있는데요. 
// 예를 들면, 아래와 같은 코드는 정상적으로 동작합니다.
// dog의 property인 name은 정상적으로 변경하는 것입니다.
// const dog = {
//     name: "Waley",
//     bread : "something"
// };
// dog.name = "testler"

// console.log(dog)

// // 만약에 dog 객체를 다시 변경하고자 하면 에러난다
// dog = {
//     color: "red"
// };

// 결론
// 2. ES6 에서 권장하는 것은 let과 const

// es6는  let과 const를 사용해서 변수를 선언하고 사용할 것을 권장하고 있습니다.
// 좀 더 엄격한 변수인 let과 const를 사용해서 코딩을 하도록 해봐야 겠네요.





// 모듈 : 재활용성 높이고, 유지보수 쉽게 하게 여러개 파일로 분리, 
// 장점 : 코드를 개선하면 이를 사용하는 모든 app 동작이 개선된다.
// 코드 수정 시 필요한 로직을 빠르게 찾을 수 있다.
// 필요한 로직만을 로드해서 메모리 낭비를 줄인다.
// 한번 다운로드 된 모듈은 웹브라우저에 의해서 저장 되기 때문에 동일한 로직을 로드 할 때 네트워크 트래픽을 절약 할 수 있다.(브라우저에만 해당)

// 자바스크립트가 구동 되는 환경(호스트환경 / ex)node js 등등 ) 

// 프로그램은 작고 단순한 것에서 크고 복잡한 것으로 진화한다. 그 과정에서 코드의 재활용성을 높이고, 유지보수를 쉽게 할 수 있는 다양한 기법들이 사용된다. 그 중의 하나가 코드를 여러개의 파일로 분리하는 것이다. 이를 통해서 얻을 수 있는 효과는 아래와 같다.

// 자주 사용되는 코드를 별도의 파일로 만들어서 필요할 때마다 재활용할 수 있다.
// 코드를 개선하면 이를 사용하고 있는 모든 애플리케이션의 동작이 개선된다.
// 코드 수정 시에 필요한 로직을 빠르게 찾을 수 있다.
// 필요한 로직만을 로드해서 메모리의 낭비를 줄일 수 있다.
// 한번 다운로드된 모듈은 웹브라우저에 의해서 저장되기 때문에 동일한 로직을 로드 할 때 시간과 네트워크 트래픽을 절약 할 수 있다. (브라우저에서만 해당)
// 모듈이란
// 순수한 자바스크립트에서는 모듈(module)이라는 개념이 분명하게 존재하지는 않는다. 하지만 자바스크립트가 구동되는 호스트 환경에 따라서 서로 다른 모듈화 방법이 제공되고 있다. 이 수업에서는 자바스크립트의 대표적인 호스트 환경인 웹브라우저에서 로직을 모듈화하는 방법에 대해서 알아볼 것이다.

// 호스트 환경이란?

// 호스트 환경이란 자바스크립트가 구동되는 환경을 의미(구글 스프레드 시트, node js, 웹브라우저 등)한다. 자바스크립트는 브라우저를 위한 언어로 시작했지만, 더 이상 브라우저만을 위한 언어가 아니다. 예를들어 node.js는 서버 측에서 실행되는 자바스크립트다. 이 언어는 자바스크립트의 문법을 따르지만 이 언어가 구동되는 환경은 브라우저가 아니라 서버측 환경이다. 또 구글의 제품 위에서 돌아가는 Google Apps Script 역시 자바스크립트이지만 google apps script가 동작하는 환경은 구글 스프레드쉬트와 같은 구글의 제품 위이다. 여러분은 자바스크립트의 문법을 이용해서 PHP와 같은 서버 시스템을 제어(node.js)하거나 구글의 제품(Google Apps Script)을 제어 할 수 있다. 지금 당장은 어렵겠지만, 언어와 그 언어가 구동되는 환경에 대해서 구분해서 사고 할 수 있어야 한다. 이를 위해서는 다양한 언어를 접해봐야 한다.
// 모듈을 만드는 방법을 알아보기에 앞서서 모듈이 없는 상황을 가정해보자.


var circle = require('./node.circle.js');
console.log( 'The area of a circle of radius 4 is '
           + circle.area(4));