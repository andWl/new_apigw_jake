'use strict';
var helper = require('../app/helper.js');
var logger = helper.getLogger('utils');
logger.setLevel('INFO');

var missingMsg = " field is missing or Invalid in the request'";

var getErrorMessage = function(field) {
  var response = {
    result: "fail",
    message: field
  };
  return response;
}

exports.getErrorMessage = getErrorMessage;
