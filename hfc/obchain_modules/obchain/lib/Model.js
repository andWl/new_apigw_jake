'use strict';
var mongoose = require('mongoose');

//MongoDB Model
var KeyData = mongoose.model('keyDB', {
  key :{
    type:String,
    required : true
  }, 
  peer : {
    type:String,
    required : true
  }
});

module.exports = {KeyData};