'use strict';
var express = require('express');
var router = express.Router();

var invoke = require('../app/invoke-transaction.js');
var query = require('../app/query.js');

var utils = require('../app/utils.js');
var obchain = require('../obchain_modules/obchain');
var helper = require('../app/helper.js');
var logger = helper.getLogger('gateway');
logger.setLevel('DEBUG');

router.post('/chaincode/:chaincodeName', async function(req, res) {
  logger.debug(`End point : POST /chaincodes/${req.params.chaincodeName}`);
  var chaincodeName = req.params.chaincodeName;
  var fcn = req.body.fcn;
  var args = req.body.args;

  req.checkBody('fcn', 'Fcn(Function Name) is required').notEmpty();
  req.checkBody('args', 'args(Arguments) is required').notEmpty();

  var errors = req.validationErrors();
  
  if(errors) {
    res.status(400).send(errors[0]);
    return;
  }

  try {
    var location = await obchain.getPeerToInvoke(args[0]);

    let message = await invoke.invokeChaincode(location,  chaincodeName, fcn, args, req.username, req.orgname);
    if(message.success) {
      logger.debug('*** Success Invoke Transactoin');
      res.send(message.message);
    } else {
      res.status(500).send(message.message);
    }
  } catch(error) {
    res.status(500).send(error);
  }
  
});


router.post('/query/chaincode/:chaincodeName', async function(req, res) {
  logger.debug(`End point : GET /chaincodes/${req.params.chaincodeName}`);
  
  var chaincodeName = req.params.chaincodeName;
  var fcn = req.body.fcn;
  var args = req.body.args;

  req.checkParams('chaincodeName', 'ChaincodeName is required').notEmpty();
  req.checkBody('fcn', 'Fcn(Function Name) is required').notEmpty();
  req.checkBody('args', 'args(Arguments) is required').notEmpty();

  var errors = req.validationErrors();
  
  if(errors) {
    res.status(400).send(errors[0]);
    return;
  }

  try {
    var location = await obchain.getPeerToQuery(args[0]);
    if(location == undefined || location == null) {
      res.sendStatus(400);
      return;
    }

    let message = await query.queryChaincode(location, chaincodeName, args, fcn, req.username, req.orgname);
    if(message.success) {
      logger.debug('*** Success Query Transactoin');
      //key는 존재하나, value가 null 일 경우
      if(message.message == null || message.message == undefined || message.message == '') {
        res.sendStatus(400);
        return;
      }
      res.send(message.message);
      return;
    } else {
      res.sendStatus(500);
    }
  } catch(err) {
    res.status(500);
    return;
  }
});

// 이렇게 export 해놔야 밖에서 사용 가능 
module.exports = router;
