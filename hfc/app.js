'use strict';

var log4js = require ('log4js');
var logger = log4js.getLogger('App');
logger.setLevel('ERROR');



var express = require('express');
var session = require('express-session');

var path = require('path');
var expressValidator = require('express-validator');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');
var util = require('util');
var expressJWT = require('express-jwt');
var jwt = require('jsonwebtoken');
var bearerToken = require('express-bearer-token');
var compression = require('compression');

// 
var users = require('./routes/users');
var channels = require('./routes/channels');
var gateway = require('./routes/gateway');
var invokeTransaction = require('./app/invoke-transaction');
var Obchain = require('./obchain_modules/obchain');



var orgName = 'Org1';
var userName = 're.org1';

var client = {};

invokeTransaction.getClient(orgName, userName)
        .then(function(tmpClient) {
                client = tmpClient;
                //console.log('clientOrg1', client);
								global.client = client;

                for(var i = 0; i < Obchain.getNodeArray().length; i++) {
                        // peerArr[i].channel = client.getChannel(Obchain.getNode(i).getChannelName());
												Obchain.setNodeChannel(i, client.getChannel(Obchain.getNode(i).getChannelName()));
                }

                //logger.debug(peerArr);
        });



var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(compression());



//Validator
app.use(expressValidator({
	errorFormatter : function(param, msg, value) {
		 return msg;
	}
}));

// token 없이 사용가능하도록 임시 주석처리
/*
 app.set('secret', 'thisismysecret');
 app.use(expressJWT({
 	secret: 'thisismysecret'
 }).unless({
 	path: ['/api/users']
 }));

*/
app.use(bearerToken());


app.use(function(req, res, next) {
	logger.debug(' ------>>>>>> new request for %s',req.originalUrl);

        req.connection.setNoDelay(true);

	if (req.originalUrl.indexOf('/api/users') >= 0) {
		return next();
	}

  // token 없이 사용가능하도록 임시 주석처리
 /*
	 var token = req.token;
	 jwt.verify(token, app.get('secret'), function(err, decoded) {
	 	if (err) {
	 		res.send({
	 			success: false,
	 			message: 'Failed to authenticate token. Make sure to include the ' +
	 				'token returned from /users call in the authorization header ' +
	 				' as a Bearer token'
	 		});
	 		return;
	 	} else {
	 		// add the decoded user name and org name to the request object
	 		// for the downstream code to use
	 		req.username = decoded.username;
	 		req.orgname = decoded.orgName;
	 		logger.debug(util.format('Decoded from JWT token: username - %s, orgname - %s', decoded.username, decoded.orgName));
	 		return next();
	 	}
	 });
*/

	req.username = userName;
	req.orgname = orgName;
	return next();
});


// 이 경로에로 들어오면 users 로가고 
app.use('/api/users', users);
app.use('/api/channels', channels);


// 이렇게 제일 아래에 설정 해놓은 이유는 처음부터 gateway로 들어오는걸 막기 위해서 
app.use('/', gateway);
// console.log(require('v8').getHeapStatistics().heap_size_limit);

// //var heapdump = require('heapdump');

// console.log('execArgv : ' + process.execArgv);

module.exports = app;
