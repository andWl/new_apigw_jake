module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [

    // First application
    {
      name      : 'API-GW',
      script    : './bin/www',
      instances : 'max',
      exec_mode : 'cluster',
      node_args : '',
      env: {
	NODE_ENV: "production",
      },
      env_production : {
        NODE_ENV: 'production'
      }
    }

  ]
};
